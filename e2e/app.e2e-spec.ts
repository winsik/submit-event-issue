import { SubmitIssuePage } from './app.po';

describe('submit-issue App', function() {
  let page: SubmitIssuePage;

  beforeEach(() => {
    page = new SubmitIssuePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
