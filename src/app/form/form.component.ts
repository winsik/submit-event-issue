import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Output() submit = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  submitButtonClicked() {
    this.submit.emit("event from emitter");
  }
}
